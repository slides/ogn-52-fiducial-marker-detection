import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.js';
import Notes from 'reveal.js/plugin/notes/notes.js';
import Highlight from 'reveal.js/plugin/highlight/highlight.js';

let deck = new Reveal({
   plugins: [ Markdown, Notes, Highlight ],
   controlsTutorial: false,
   hash: true
})
deck.initialize();
